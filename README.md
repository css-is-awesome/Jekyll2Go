## Jekyll on GitLab

### Running on GitLab
Some inspiration came from the [Build Jekyll with Bundler][s1] repository, which
already touches upon use of docker images and such. I have not yet investigated
if I can use my docker composition for the CI build on GitLab.

See the `.gitlab-ci.yml` file for the specific configuration options on how I'm
now building on GitLab. The produced artifacts are uploaded to an S3 bucket,
from where it's being served as a static website by AWS (working on CloudFront
to provide HTTP/SSL connectivity and CDN).

### Running on dev machine
See the `docker-compose.yml` file for specifics.
- Install [Docker][t1].
- Run `docker-compose up`
- Browse to [Localhost:4000][l1]

Docker image is set to live-reload, so start hacking away in the source folder
give it a pause, refresh the browser, and bob's your uncle!

### Community

The official community support is available here: http://jekyllrb.com/help/

### Bug Reports

Please report bugs or log feature requests using Github Issues, pull requests are welcome.

[s1]: https://gitlab.com/jekyll-themes/default-bundler
[t1]: https://www.docker.com/get-docker
[l1]: http://0.0.0.0:4000/
