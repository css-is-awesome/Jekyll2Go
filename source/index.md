---
title: Home
layout: home
pinned: home
---

## Windgazer.nl

I am a freelancer, a traveler, a musician, a salsa-dancer, ballroom dancer and I like to
practice martial arts, listen to jazz, pretend to be a photographer, play games
(computer and/or table-top), read books watch movies and drinking rum. On this website you
will mostly find references to what I do as a profession. For the more eclectic mix of my
interests I would  recommend you have a gander at [the insignificant pebble][p], where any
of my interests may find a voice!

Testing

[1]: http://jekyllrb.com/
[p]: http://pebble.windgazer.nl/
