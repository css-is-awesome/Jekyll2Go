# SASS Tricks

This should simple be an easy to use repository of partials that can be thrown at any project using bower, or good old copy/paste. It's main repository can be found on github at
[https://github.com/Windgazer-Freelance/sasstricks][duh].

A stripped-down version of the original sasstricks was left in this repository
mainly to show howto effectively make use of existing css-libs in this
quickstart Jekyll environment...

[duh]: https://github.com/Windgazer-Freelance/sasstricks
