---
title: About
tags:
  - about
  - info
  - personal
---

## About {{site.title}}

All solutions are published under '[☺ License][l]' unless indicated otherwise. All
publications remain copyrighted by `{{site.author}}` and a written acknowledgement is required to use
these materials for any purpose other than personal archiving for later use.

[l]: /LICENSE
