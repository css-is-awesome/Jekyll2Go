In this folder you can drop redirects, providing an alternate entry-point to
content on your website. Useful for redirecting old URL's (from a previous
implementation, for instance) to the new location of the content.

To use this create a `.md` file in any folder (but best to group it here):
***
---
layout: redirect
metarefresh: /about
permalink: /whoami
---
***

The `permalink` is where jekkyl will generate the redirect and the `metarefresh`
is the location you'll redirect to. This location is also set as the
`canonical` path so that google will not, or no longer, keep the permalink in
it's search results.
