---
title: Contact
pinned: gmaps
permalink: /contact.html
---

## Contact

Should you have need to contact me, it's always best to pick up to phone, should you
already have that number available. Under other circumstances the following might help:

<ol id="hcard-Martin-Reurings" class="vcard">
    <li class="fn">Martin Reurings</li>
    <li class="org">Windgazer</li>
    <li><a class="email" href="mailto:martin@windgazer.nl">martin@windgazer.nl</a></li>
    <li><ol class="adr">
        <li><a href="#gmapsFrame" class="street-address">Raadhuisstraat 38</a></li>
        <li><span class="postal-code">3648AS</span>, <span class="locality">Wilnis</span></li>
        <li class="country-name">The Netherlands</li>
    </ol></li>
</ol><article id="gmapsFrame">
    <a href="#close" class="closer">Close</a>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1222.7836456166606!2d4.9030043184757295!3d52.19673764650604!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c6744811480341%3A0x221f3e76523414bb!2sWindgazer!5e0!3m2!1sen!2sus!4v1440540631240" width="512" height="400" frameborder="0" style="border:0;" allowfullscreen></iframe>
</article>

### Quick Question

For a quick question, you can fill in the form below and I'll get back to you as soon as
I read the message.

<form action="https://docs.google.com/forms/d/1TvNXoRutSXkgQS5PmAmjmo9aiG_q20_YQUcZehD4t84/formResponse" method="POST" id="contactForm" onsubmit=""><ol>
    <li><label for="i1">Name</label><input id="i1" type="text" name="entry.1905715123" placeholder="Justin Case" required /></li>
    <li><label for="i2">E-Mail</label><input id="i2" type="email" name="entry.2073784579" placeholder="johnsmith@anonymous.net" required/></li>
    <li><label for="t1">Question</label><textarea id="t1" name="entry.2121724557" rows="8" cols="0" placeholder="If you give someone a program, you will frustrate them for a day; if you teach them how to program, you will frustrate them for a lifetime." required></textarea></li>
    <li><input type="submit" name="submit" value="Submit"></li>
</ol></form>


<style>
    #gmapsFrame {
        height: 0;
        overflow: hidden;
        transition: height 300ms;
    }
    #gmapsFrame:target {
        padding-top: 4em;
        margin-top: -4em;
        height: 450px;
    }
</style>

<script>
    ( function setupSubmitByAjax() {
        var form = document.querySelector( "#contactForm" );
        var msg = document.createElement("p");
        msg.innerHTML = "<em>If you see this message, your question should be in my mailbox :)</em>";
        //If anything fails in this method, presumably the form will get submitted old-style
        form.addEventListener( "submit", function formSubmitEvent ( e ) {
            if ( FormData ) {
                var request = new XMLHttpRequest();
                request.open("POST", form.action);
                request.send(new FormData(form));
                console.log("Don't worry about the 'Access-Control-Allow-Origin' warning, it is to be expected!");
                form.parentNode.replaceChild( msg, form );
                e.preventDefault();
                e.stopPropagation();
                form.reset();
                return false;
            }
            return true;
        } );
    } )();
</script>
