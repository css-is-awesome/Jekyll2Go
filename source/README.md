WindgazerNL
===================

An attempt at building my freelance persona using Jeckyll.

To build run `jekyll serve --drafts -w --config _config.yml,_config-dev.yml`

Requires:
    - Ruby
    - Jeckyll
    - Compass
    - Node
